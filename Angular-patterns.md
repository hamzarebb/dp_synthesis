# Integration des designs patterns avec Angular

## Patrons de Création
- **Singleton** :
Ce pattern permet de garantir qu'il n'y a qu'une seule instance de votre classe.

Cela fonctionne bien lors de la création de services qui devraient avoir un seul état. 

Angular utilise ce pattern pour assurer qu'il n'y a qu'une seule instance des services partagés dans toute l'application, ce qui permet d'éviter les problèmes liés à la gestion des états.


```typescript
@Injectable()
export class AuthenticationService {
  private isLoggedIn = false;

  login() {
    this.isLoggedIn = true;
  }

  logout() {
    this.isLoggedIn = false;
  } 
}

```

## Patrons structurels
- **Décorateur** :
Ce pattern est une alternative aux sous-classes pour étendre un objet en utilisant la composition au lieu de l'héritage. 

En fait, le décorateur attache des responsabilités supplémentaires à un objet. Le concept principal est d'avoir un objet qui enveloppe un autre objet. 

Celui qui enveloppe l'objet est le décorateur et c'est le même type de l'objet d'origine mais il a aussi un objet du même type de l'objet.

Angular utilise ce pattern pour étendre ou modifier le comportement des classes et des méthodes sans les modifier directement, ce qui favorise la flexibilité et la réutilisabilité du code.


```typescript
@Component({
  selector: 'app-store',
  template: '<p>Decorated Component</p>',
})
export class StoreComponent {

}
```

## Patrons comportementaux
- **Observateur** :
Le modèle d’observateur permet le partage de données et la communication axée sur les événements entre les composants. 

Angular utilise ce pattern pour implémenter la liaison de données entre les vues et les modèles de données, permettant des mises à jour automatiques de l'interface utilisateur en fonction des changements de données.

```typescript
export class DataService {
  private data: string;
  private observers: Observer[] = [];

  add(observer: Observer) {
    this.observers.push(observer);
  }

  notify() {
    this.observers.forEach(observer => observer.update(this.data));
  }
}
```

Angular intègre ces exemples de design patterns et bien d'autres de manière transparente dans son architecture, ce qui facilite le développement d'applications web robustes et maintenables